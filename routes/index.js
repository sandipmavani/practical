const express = require('express');
const routes = express.Router();
const region = require('./region');
const user = require('./user');
const { authMiddleware } = require('../middleware/user');

routes.use('/region', authMiddleware, region);

routes.use('/user', user);


module.exports = routes;
